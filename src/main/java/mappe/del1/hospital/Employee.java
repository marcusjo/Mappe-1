package mappe.del1.hospital;

/**
 * @author marcusjohannessen
 */

public class Employee extends Person {

    //Default constructor
    public Employee(){
        super("","","");
    }

    public Employee(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}