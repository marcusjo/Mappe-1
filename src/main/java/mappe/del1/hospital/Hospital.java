package mappe.del1.hospital;

import java.util.ArrayList;

/**
 * @author marcusjohannessen
 */

public class Hospital {
    private String hospitalName;
    private ArrayList<Department> departments;

    /**
     *
     * @param hospitalName
     */

    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
        this.departments = new ArrayList<>();
    }

    public String getHospitalName() {
        return hospitalName;
    }

    //Should not return a clone list!!
    public ArrayList<Department> getDepartments() {
        return departments;
    }

    /**
     *
     * @param department
     * Throws IllegalArgumentException if department already exists
     */
    public void addDepartment(Department department){
        if(departments.contains(department)){
            throw new IllegalArgumentException("Department already exist");
        }
        departments.add(department);
    }

    @Override
    public String toString() {
        return "\nHospital" +
                "\nHospitalName='" + hospitalName +
                "\nDepartments=" + departments;
    }
}
