package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Employee;
import mappe.del1.hospital.Patient;

/**
 * @author marcusjohannessen
 */

public abstract class Doctor extends Employee {

    public Doctor(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @param patient
     * @param diagnosis
     * takes in a patient an give a diagnosis
     * only objects of type Doctor can give a diagnosis
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);
}