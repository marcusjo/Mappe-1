package mappe.del1.hospital.healthpersonal.doctor;

import mappe.del1.hospital.Patient;

/**
 * @author marcusjohannessen
 */

public class GeneralPractitioner extends Doctor {

    //Default constructor
    public GeneralPractitioner(){
        super("","","");
    }

    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     *
     * @param patient
     * @param diagnosis
     * if patients exists set diagnosis
     */
    public void setDiagnosis(Patient patient, String diagnosis){
        if(patient == null || diagnosis == null && diagnosis.equals("")){
            throw new IllegalArgumentException("No patient or no diagnosis detected");
        }else {
            patient.setDiagnosis(diagnosis);
        }
    }
}