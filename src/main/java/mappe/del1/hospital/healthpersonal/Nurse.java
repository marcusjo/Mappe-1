package mappe.del1.hospital.healthpersonal;

import mappe.del1.hospital.Employee;

/**
 * @author marcusjohannessen
 */

public class Nurse extends Employee {

    public Nurse(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("Profession: Nurse");
        return sb.toString();
    }
}