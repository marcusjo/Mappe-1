package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Objects;

/**
 * @author marcusjohannessen
 */

public class Department {
    private String departmentName;
    private ArrayList<Patient> patients;
    private ArrayList<Employee> employees;

    public Department(String departmentName) {
        this.departmentName = departmentName;
        this.patients = new ArrayList<>();
        this.employees = new ArrayList<>();

    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public ArrayList<Employee> getEmployees() {
        return employees;
    }

    /**
     * @param employee
     * @return false if employee exists, true emolployee have been added
     */
    public void addEmployee(Employee employee) {
        for (Employee employee1 : employees) {
            if (employee1.getSocialSecurityNumber().equals(employee.getSocialSecurityNumber())) {
                throw new IllegalArgumentException("Employee already exists in register");
            }
        }
        employees.add(employee);
    }

    /**
     * @param patient
     * @return false if patient exists in patients list, true if patient added to list
     */
    public void addPatient(Patient patient) {
        for (Patient patient1 : patients) {
            if (patient1.getSocialSecurityNumber().equals(patient.getSocialSecurityNumber())) {
                throw new IllegalArgumentException("Patient already exists in register");
            }
        }
        patients.add(patient);
    }

    /**
     * @param person Removed person from the list person is in
     *               if person do not exist in any register the throw exception.
     *               Exception is type checked because it handles the exception during
     *               runtime and handles it.
     */


    public void removePerson(Person person) throws RemoveException {
        //If person is a patient
        if (person instanceof Patient) {
            Patient patient = (Patient) person;
            if (!patients.contains(patient)) {
                throw new RemoveException(" Person do not exist in patients register");
            }
            patients.remove(patient);
            System.out.println(patient.getFirstName() + " Successfully removed from patients");
        }
        //If person is an employee
        else if (person instanceof Employee) {
            Employee employee = (Employee) person;
            if (!employees.contains(employee)) {
                throw new RemoveException("Employee do not exist in employees register");
            }
            employees.remove(employee);
            System.out.println(employee.getFirstName() + " Successfully removed from employees");
        }
    }

    /**
     * @param o
     * @return true if object name equals name compared to, else it will return false
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Department)) {
            return false;
        }
        Department that = (Department) o;
        return Objects.equals(departmentName, that.departmentName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(departmentName, patients, employees);
    }

    @Override
    public String toString() {
        return "\nDepartment" +
                "\nDepartmentName: " + departmentName +
                "\nPatients: " + patients +
                "\nEmployees: " + employees;
    }
}
