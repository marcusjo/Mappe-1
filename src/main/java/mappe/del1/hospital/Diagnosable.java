package mappe.del1.hospital;

/**
 * @author marcusjohannessen
 */

public interface Diagnosable {

    void setDiagnosis(String diagnosis);
}
