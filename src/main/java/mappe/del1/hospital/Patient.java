package mappe.del1.hospital;

/**
 * @author marcusjohannessen
 */

public class Patient extends Person implements Diagnosable {
    private String diagnosis;

    //Default constructor
    public Patient(){
        super("","","");
    }

    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
        this.diagnosis = "";
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(super.toString());
        sb.append("\nDescription: Patient\nDiagnosis: " + diagnosis);
        return sb.toString();
    }
}