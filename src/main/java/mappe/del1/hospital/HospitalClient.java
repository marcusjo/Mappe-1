package mappe.del1.hospital;

/**
 * @author marcusjohannessen
 * Main class that test to see if program run suexsistingemployeecessfully
 */

public class HospitalClient {
    public static void main(String[] args) {
        Hospital hospital = new Hospital("St.Olavs Hospital");
        HospitalTestData.fillRegisterWithTestData(hospital);
        System.out.println("Test to see if it works\n" + hospital.getDepartments().get(1).getPatients());


        try{
            Employee existingEmployee = hospital.getDepartments().get(0).getEmployees().get(1);
            //prints if person is successfully removed
            hospital.getDepartments().get(0).removePerson(existingEmployee);

            Employee notInlistEmployee = new Employee("Not", "In", "List");
            //Throws RemoveException with message: "employee do not exist in employees register
            hospital.getDepartments().get(0).removePerson(notInlistEmployee);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}