package mappe.del1.hospital.exception;

import java.io.Serial;
import java.io.Serializable;

/**
 * @author marcusjohannessen
 * A class that handles exceptions
 * Is beeing thrown in department class
 * removePerson()
 */

public class RemoveException extends Exception implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    public RemoveException(final String message) {
        super(message);
    }
}