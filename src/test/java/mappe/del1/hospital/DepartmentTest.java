package mappe.del1.hospital;

import mappe.del1.hospital.exception.RemoveException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

/**
 * @author marcusjohannessen
 */

class DepartmentTest {

    @Nested
    class testOfRemoverPersonMethod {

        @Test
        @DisplayName("Test if removePerson() method do not throw an Exception")
        void removePersonNotThrows() {
            Hospital hospital = new Hospital("Test hospital");
            Department dep = new Department("Test");
            hospital.addDepartment(dep);
            Patient patient = new Patient("test", "test", "123");
            dep.addPatient(patient);
            Assertions.assertDoesNotThrow(()-> dep.removePerson(patient));
        }

        @Test
        @DisplayName("Test to see if removePerson() throw an Exception")
        void removePersonThrows() {
            Hospital hospital = new Hospital("Test hospital");
            Department dep = new Department("Test");
            hospital.addDepartment(dep);
            Patient patient = new Patient("test", "test", "123");
            //patient is not added to any register, so should throw an exception
            Assertions.assertThrows(RemoveException.class, () -> dep.removePerson(patient));
        }
    }
}